import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  final options = ['Um', 'Dois', 'Três', 'Quatro', 'Cinco'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Componentes Temp')),
      body: ListView(children: _createItensShort()),
    );
  }

  List<Widget> _createItens() {
    List<Widget> list = new List<Widget>();
    for (var opt in options) {
      final tempWidget = ListTile(title: Text(opt));
      list..add(tempWidget)..add(Divider());
    }
    return list;
  }

  List<Widget> _createItensShort() => options
      .map((item) => Column(
            children: [
              ListTile(
                title: Text(item + '!'),
                subtitle: Text('Qualquer coisa'),
                leading: Icon(Icons.account_balance_wallet),
                trailing: Icon(Icons.keyboard_arrow_right),
                onTap: () {},
              ),
              Divider()
            ],
          ))
      .toList();
}
